$(document).ready(function () {
    $('body').on('click', '.btn-ip-search', function (e) {
        let btn = $(this);
        let ip = $('#ip-form .ip-value').val();
        $('.ip-value-error').html('');
        // $(btn).buttonLoader('start');
        let request = BX.ajax.runComponentAction('ruba:ip.lookup', 'requestIp', {
            mode: 'class',
            data: {
                ip: ip
            }
        });
        request.then(function (response) {
            // $(btn).buttonLoader('stop');
            console.log(response);
            if (response.status === 'success') {
                if (response.data.status === false) {
                    $('.ip-value-error').html(response.data.message);
                    return false;
                }
                $('.ip-data').html(response.data);
            } else {
                $('.ip-value-error').html('Ошибка при запросе информации об ip, попробуйте позже');
            }
        });
        e.preventDefault();
    });
});