<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<form id='ip-form'>
    <div class="input-group mb-3">
        <input type="text" class="form-control ip-value" placeholder="ip адрес" aria-label="ip адрес"
            aria-describedby="ip" value='<?=$arResult['ip']?>'>
        <button class="btn btn-primary btn-ip-search" type="button" id="ip">look up</button>
        <div class="invalid-feedback ip-value-error"></div>
    </div>
</form>
<div class='ip-data'>
    <?=$arResult['ipData'];?>
</div>