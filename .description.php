<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = [
    'NAME' => 'Информация об ip адресе',
    'DESCRIPTION' => 'Определяет географическое положение по IP адресу'
];
?>