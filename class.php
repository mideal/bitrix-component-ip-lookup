<?php

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

use Bitrix\Main\ArgumentException;
use Bitrix\Main\Engine\Contract\Controllerable;
use Bitrix\Main\Engine\ActionFilter;
use Bitrix\Main\Loader;
use Bitrix\Highloadblock as HL;
use Bitrix\Main\LoaderException;
use Bitrix\Main\ObjectPropertyException;
use Bitrix\Main\SystemException;
use Bitrix\Main\Web\HttpClient;
use Bitrix\Main\Mail\Event;

class IpLookup extends CBitrixComponent implements Controllerable
{
    private string $api = 'https://api.sypexgeo.net/json/';
    private string $ip;
    private string $hlEntity;

    /**
     * @throws LoaderException
     * @throws ArgumentException
     * @throws ObjectPropertyException
     * @throws SystemException
     */
    public function onPrepareComponentParams($arParams)
    {
        Loader::includeModule('highloadblock');
        $hlblock = HL\HighloadBlockTable::getById(1)->fetch();
        $entity = HL\HighloadBlockTable::compileEntity($hlblock);
        $this->hlEntity = $entity->getDataClass();
    }

    public function configureActions(): array
    {
        return [
            'requestIp' => [
                'prefilters' => [
                    new ActionFilter\HttpMethod(
                        [ActionFilter\HttpMethod::METHOD_GET, ActionFilter\HttpMethod::METHOD_POST]
                    ),
                    new ActionFilter\Csrf(),
                ],
                'postfilters' => []
            ],
        ];
    }

    public function executeComponent()
    {
        $this->arResult['ip'] = '';
        $this->arResult['ipData'] = '';
        $params = $this->request->getValues();
        if ($params['ip'] && filter_var($params['ip'], FILTER_VALIDATE_IP)) {
            $this->ip = $params['ip'];
            $this->arResult['ip'] = $params['ip'];
            $this->arResult['ipData'] = $this->getIpData();
        }
        $this->includeComponentTemplate();
    }

    public function requestIpAction(string $ip)
    {
        if (!filter_var($ip, FILTER_VALIDATE_IP)) {
            return ['status'=>false, 'message'=>'Введен некорректный ip адрес'];
        }
        $this->ip = $ip;
        return $this->getIpData();
    }

    public function getIpData()
    {
        try {
            $ipData = $this->getIpDataFromDb();
            if ($ipData) {
                return $ipData;
            }
            $ipData = $this->getIpDataFromApi();
        } catch (\Throwable $th) {
            Event::send([
                'EVENT_NAME' => 'IP_ERROR',
                'LID' => SITE_ID,
                'C_FIELDS' => [
                    'MESSAGE' => $th->getMessage(),
                ],
            ]);
            return false;
        }

        return $ipData;
    }

    public function getIpDataFromDb()
    {
        $ipData = $this->hlEntity::getRow([
            'select' => ['UF_DATA'],
            'filter' => ['UF_IP'=>$this->ip]
        ]);

        return $ipData['UF_DATA'] ?? false;
    }

    public function getIpDataFromApi()
    {
        $url = $this->api.$this->ip;
        $httpClient = new HttpClient();
        $ipData = $httpClient->get($url);
        $hlIpData = [
            'UF_IP'=>$this->ip,
            'UF_DATA'=>$ipData
        ];
        $this->hlEntity::add($hlIpData);
        return $ipData;
    }
}
